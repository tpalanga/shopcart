name := "ShopCart"

version := "1.0-SNAPSHOT"

scalaVersion := "2.11.7"

scalacOptions += "-deprecation"

libraryDependencies ++= Seq(
  "org.specs2" %% "specs2-core" % "3.6.5" % "test"  
)

coverageExcludedFiles := """.*Main.*"""
