package shopcart

import ShopCartDefs._

trait ShopCart {
  val scannedItems: List[Item]
  val shoppingCart: Cart
  val total: BigDecimal
}
