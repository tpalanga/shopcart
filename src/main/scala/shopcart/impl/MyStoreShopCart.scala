package shopcart.impl

import shopcart.ShopCart
import shopcart.ShopCartDefs._
import shopcart.impl.MyStoreShopCart._

object MyStoreShopCart {
  sealed trait MyStoreItem extends Item
  case object Orange extends MyStoreItem { val price = BigDecimal(0.25) }
  case object Apple extends MyStoreItem { val price = BigDecimal(0.60) }

  case object AppleDiscount extends Discount {
    def calculate(cart: Cart): BigDecimal = {
      val quantity = cart.getOrElse(Apple, 0)
      Apple.price * (quantity / 3) * -1
    }
  }
  case object OrangeDiscount extends Discount {
    def calculate(cart: Cart): BigDecimal = {
      val quantity = cart.getOrElse(Orange, 0)
      Orange.price * (quantity / 2) * -1
    }
  }

  def fromString: PartialFunction[String, MyStoreItem] = {
    case "Orange" => Orange
    case "Apple" => Apple
  }

  def fromInput(input: List[String]): MyStoreShopCart = {
    val items = input collect fromString
    MyStoreShopCart(items)
  }
}

case class MyStoreShopCart(scannedItems: List[MyStoreItem]) extends ShopCart {
  val currentDiscounts = List(AppleDiscount, OrangeDiscount)

  val shoppingCart: Cart = scannedItems.groupBy(i => i).map { case (k, v) => k -> v.size }

  val total: BigDecimal = {
    val totalPrice = shoppingCart.foldLeft(BigDecimal(0))((acc, elem) => acc + elem._1.price * elem._2)
    val discountTotal = currentDiscounts.foldLeft(BigDecimal(0)){ (acc, disc) => acc + disc.calculate(shoppingCart) }
    totalPrice + discountTotal
  }
}
