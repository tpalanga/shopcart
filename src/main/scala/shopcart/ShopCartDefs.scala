package shopcart

object ShopCartDefs {
  trait Item {
    val price: BigDecimal
  }

  type Cart = Map[Item, Int]

  trait Discount {
    def calculate(cart: Cart): BigDecimal
  }
}
