package shopcart

import scala.io.StdIn
import shopcart.impl.MyStoreShopCart

object Main extends App {

  println("Please provide the shopping cart contents on a single line separated by spaces")
  val inputStr = StdIn.readLine()
  val input = inputStr.split(" ").toList

  val cart = MyStoreShopCart.fromInput(input)

  println(s"Your ShoppingCart contents: ${cart.shoppingCart}")
  println(s"Your ShoppingCart total: ${cart.total}")

}