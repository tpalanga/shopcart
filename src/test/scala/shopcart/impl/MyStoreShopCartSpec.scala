package shopcart.impl

import org.specs2.mutable.Specification
import shopcart.ShopCartDefs.Item
import shopcart.impl.MyStoreShopCart._


class MyStoreShopCartSpec  extends Specification {
  "MyStoreShopCart companion" should {
    "create MyStoreShopCart from list of String" in {
      val input = List("Apple", "Orange", "Apple", "Apple")
      MyStoreShopCart.fromInput(input) ===  MyStoreShopCart(List(Apple, Orange, Apple, Apple))
    }

    "create MyStoreShopCart from list of String should ignore invalid items" in {
      val input = List("Apple", "Orange", "Invalid", "Apple", "Apple", "Unknown")
      MyStoreShopCart.fromInput(input) ===  MyStoreShopCart(List(Apple, Orange, Apple, Apple))
    }
  }


  "MyStoreShopCart" should {
    "return the shopping cart groupped" in {
      val myCart = MyStoreShopCart(List(Apple, Orange, Apple, Apple))
      myCart.shoppingCart === Map[Item, Int](Apple -> 3, Orange -> 1)
    }

    "calculate total without applicable discounts" in {
      val myCart = MyStoreShopCart(List(Apple, Orange, Apple))
      myCart.total === BigDecimal(1.45)
    }

    "calculate total with applicable discounts" in {
      val myCart = MyStoreShopCart(List(Apple, Orange, Apple, Apple, Orange))
      myCart.total === BigDecimal(1.45)
    }

    "calculate total with no Apples" in {
      val myCart = MyStoreShopCart(List(Orange, Orange))
      myCart.total === BigDecimal(0.25)
    }

    "calculate total with no Oranges" in {
      val myCart = MyStoreShopCart(List(Apple, Apple, Apple))
      myCart.total === BigDecimal(1.20)
    }

  }
}
